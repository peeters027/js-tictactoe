describe("Check result - should:  ", function () {
    it("detect win when X is an active player", function () {
        board[0] = 'X';
        board[3] = 'X';
        board[6] = 'X';
        expect(checkWin('X')).toBe(true);
    });

    it("detect win when X is an active player", function () {
        board[1] = 'X';
        board[4] = 'X';
        board[7] = 'X';
        expect(checkWin('X')).toBe(true);
    });

    it("detect win when X is an active player", function () {
        board[2] = 'X';
        board[5] = 'X';
        board[8] = 'X';
        expect(checkWin('X')).toBe(true);
    });

    it("detect win when X is an active player", function () {
        board[0] = 'X';
        board[1] = 'X';
        board[2] = 'X';
        expect(checkWin('X')).toBe(true);
    });

    it("detect win when X is an active player", function () {
        board[3] = 'X';
        board[4] = 'X';
        board[5] = 'X';
        expect(checkWin('X')).toBe(true);
    });

    it("detect win when X is an active player", function () {
        board[6] = 'X';
        board[7] = 'X';
        board[8] = 'X';
        expect(checkWin('X')).toBe(true);
    });

    it("detect win when X is an active player", function () {
        board[0] = 'X';
        board[4] = 'X';
        board[8] = 'X';
        expect(checkWin('X')).toBe(true);
    });

    it("detect win when X is an active player", function () {
        board[6] = 'X';
        board[4] = 'X';
        board[2] = 'X';
        expect(checkWin('X')).toBe(true);
    });

    it("not detect win when X is an active player", function () {
        board = getExampleBoardForTie();
        expect(checkWin('X')).toBe(false);
    });

    it("detect win when O is an active player", function () {
        board[0] = 'O';
        board[3] = 'O';
        board[6] = 'O';
        expect(checkWin('O')).toBe(true);
    });

    it("detect win when O is an active player", function () {
        board[1] = 'O';
        board[4] = 'O';
        board[7] = 'O';
        expect(checkWin('O')).toBe(true);
    });

    it("detect win when O is an active player", function () {
        board[2] = 'O';
        board[5] = 'O';
        board[8] = 'O';
        expect(checkWin('O')).toBe(true);
    });

    it("detect win when O is an active player", function () {
        board[0] = 'O';
        board[1] = 'O';
        board[2] = 'O';
        expect(checkWin('O')).toBe(true);
    });

    it("detect win when O is an active player", function () {
        board[3] = 'O';
        board[4] = 'O';
        board[5] = 'O';
        expect(checkWin('O')).toBe(true);
    });

    it("detect win when O is an active player", function () {
        board[6] = 'O';
        board[7] = 'O';
        board[8] = 'O';
        expect(checkWin('O')).toBe(true);
    });

    it("detect win when O is an active player", function () {
        board[0] = 'O';
        board[4] = 'O';
        board[8] = 'O';
        expect(checkWin('O')).toBe(true);
    });

    it("detect win when O is an active player", function () {
        board[6] = 'O';
        board[4] = 'O';
        board[2] = 'O';
        expect(checkWin('O')).toBe(true);
    });

    it("not detect win when O is an active player", function () {
        board = getExampleBoardForTie();
        expect(checkWin('O')).toBe(false);
    });

    it("not detect win when there is invalid argument for check win", function () {
        board = getExampleBoardForTie();
        expect(checkWin('invalid input')).toBe(false);
    });

    it("not detect win when there is invalid argument for check win", function () {
        board = getExampleBoardForTie();
        expect(checkWin(1)).toBe(false);
    });

    it("detect draw", function () {
        board = getExampleBoardForTie();
        expect(checkTie()).toBe(true);
    });

    it("not detect draw", function () {
        board = getExampleBoardForTie();
        board[0] = undefined;
        expect(checkTie()).toBe(false);
    });
});

describe("Validate move - should:  ", function () {
    stateOfGame = true;
    id = 0;
    it("return false if move is invalid", function () {
        board[0] = 'X';
        expect(validateMove(id)).toBe(false);
    });

    it("return true if move is correct", function () {
        board[0] = undefined;
        expect(validateMove(id)).toBe(true);
    });
});

describe("Get active player - should:  ", function () {
    it("return X as an active player", function () {
        isItXTurn = true;
        expect(getActivePlayer(isItXTurn)).toBe('X');
    });
    it("return O as an active player", function () {
        isItXTurn = false;
        expect(getActivePlayer(isItXTurn)).toBe('O');
    });

});

describe("Update statistics - should ", function () {
    playerXScore = 0;
    playerOScore = 0;
    draws = 0;
    matches = 0;
    xAsActivePlayer = 'X';
    oAsActivePlayer = 'O';
    updateWinningResults(xAsActivePlayer);
    it("increment number of matches during updating statistics", function () {
        expect(matches).toBe(1);
    });
    it("increment number of winning's player score during updating statistics", function () {
        expect(playerXScore).toBe(1);
    });
    it("increment number of winning's player score during updating statistics", function () {
        updateWinningResults(oAsActivePlayer);
        expect(playerOScore).toBe(1);
    });
    it("increment number of draws player score during updating statistics", function () {
        updateDrawResults();
        expect(draws).toBe(1);
    });
});

describe("Reseting game settings - should:  ", function () {
    it("activate state of game", function () {
        stateOfGame = false;
        resetGameSettings();
        expect(stateOfGame).toBe(true);
    });
    it("reset number of moves", function () {
        numberOfMoves = 10;
        resetGameSettings();
        expect(numberOfMoves).toBe(0);
    });
    it("reset move history", function () {
        moveHistory[0] = '1';
        resetGameSettings();
        expect(moveHistory).toEqual([ ]);
    });
    it("reset board", function () {
        board[0] = 'X';
        resetGameSettings();
        expect(board).toEqual([ ]);
    });
});

describe("Switch turn - should:  ", function () {
    it("switch state of active player", function () {
        isItXTurn = false;
        switchTurn();
        expect(isItXTurn).toBe(true);
    });
});



describe("Undo - should:  ", function () {
    var dummyElement = document.createElement('div');
    document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);
    it("delete the last element of move history", function () {
        numberOfMoves = 1;
        stateOfGame = true;
        moveHistory[0] = '1';
        board[0] = 'X';
        undo();
        expect(moveHistory).toEqual([ ]);
    });
    it("delete the last element of board", function () {
        numberOfMoves = 1;
        stateOfGame = true;
        moveHistory[0] = '1';
        id = 1;
        board[1] = 'X';
        undo();
        expect(board[1]).toEqual(undefined);
    });
});



function getExampleBoardForTie() {
    board[0] = 'X';
    board[1] = 'X';
    board[2] = 'O';
    board[3] = 'O';
    board[4] = 'O';
    board[5] = 'X';
    board[6] = 'X';
    board[7] = 'O';
    board[8] = 'O';
    return board;
}


