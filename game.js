var gameTitle = "TIC-TAC-TOE",
    statisticsName = "STATISTICS",
    playerXScore = 0,
    playerOScore = 0,
    numberOfMoves = 0,
    moveHistory = [],
    stateOfGame = false,
    isItXTurn = true,
    draws = 0,
    matches = 0,
    activePlayer = 'X',
    board = new Array(9);

function start() {
    getTitle();
    getSettingsForStartingPlayer();
    loadStatistics();
    getSquares();
}

function getMessage(activePlayer) {
    document.getElementById("message").innerHTML = activePlayer + "'s turn.";
}

function getTitle() {
    document.getElementById("gamename").innerHTML = gameTitle;
}

function getStatisticsName() {
    document.getElementById("statistics").innerHTML = statisticsName;
}

function getFirstPlayerInfo() {
    document.getElementById("XPlayer").innerHTML = "X Player score: " + getFirstPlayerScore();
}

function getSecondPlayerInfo() {
    document.getElementById("OPlayer").innerHTML = "O Player score: " + getSecondPlayerScore();
}

function getDraws() {
    document.getElementById("Draws").innerHTML = "Draws: " + draws;
}

function getMatches() {
    document.getElementById("Matches").innerHTML = "Matches: " + matches;
}

function getSquares() {
    var squares = "";
    for (i = 0; i < 9; i++) {
        squares = squares + '<div class="square" id = ' + i + ' onclick = "makeMove(this)"></div>';
        if ((i + 1 % 3) == 0) squares = '<div style = "clear:both"></div>';
    }
    document.getElementById("gametable").innerHTML = squares;
}

function makeMove(square) {
    if (validateMove(square.id)) {
        var activePlayer = getActivePlayer(isItXTurn);
        moveHistory[numberOfMoves] = square.id;
        numberOfMoves++;
        square.innerText = activePlayer;
        board[square.id] = activePlayer;
        if (checkWin(activePlayer)) {
            updateWinningResults(activePlayer);
            loadStatistics();
            setTimeout(function () { swal("Congratulations!", "Player " + activePlayer + " won!", "success") }, 50);
            stateOfGame = false;
        }
        else if (checkTie()) {
            setTimeout(function () { swal("It's a draw!", "", "info") }, 50);
            updateDrawResults();
            loadStatistics();
            stateOfGame = false;
        }
        switchTurn();
        getMessage(getActivePlayer(isItXTurn));
    }
}

function validateMove(squareId) {
    if (board[squareId] === undefined && stateOfGame === true) {
        return true;
    }
    return false;
}

function getActivePlayer(isItXTurn) {
    if (isItXTurn === false) {
        return activePlayer = 'O';
    }
    else if (isItXTurn === true) {
        return activePlayer = 'X';
    }
}

function switchTurn() {
    isItXTurn = !isItXTurn;
}
function getValueAtSquare(id) {
    return document.getElementById(id).innerText;
}

function getFirstPlayerScore() {
    return playerXScore;
}

function getSecondPlayerScore() {
    return playerOScore;
}
function updateWinningResults(activePlayer) {
    if (activePlayer === 'X') {
        playerXScore++;
        matches++;
    }
    else if (activePlayer === 'O') {
        playerOScore++;
        matches++;
    }
}

function updateDrawResults() {
    draws++;
    matches++;
}
function newGame() {
    isItXTurn = getSettingsForStartingPlayer();
    getMessage(getActivePlayer(isItXTurn));
    resetGameSettings();
    start();
}

function restart() {
    isItXTurn = getSettingsForStartingPlayer();
    getMessage(getActivePlayer(isItXTurn));
    resetGameSettings();
    resetStatistics();
    start();
}

function undo() {
    if (numberOfMoves > 0 && stateOfGame == true) {
        id = moveHistory[numberOfMoves - 1];
        numberOfMoves--;
        board[id] = undefined;
        moveHistory.pop();
        switchTurn();
        document.getElementById(id).innerHTML = "";
        getMessage(getActivePlayer(isItXTurn));
    }
}

function resetGameSettings() {
    stateOfGame = true;
    numberOfMoves = 0;
    moveHistory = [];
    board = [];
}

function resetStatistics() {
    playerXScore = 0;
    playerOScore = 0;
    matches = 0;
    draws = 0;
}

function getSettingsForStartingPlayer() {
    if (document.getElementById("X").checked == true) {
        return isItXTurn = true;
    }
    else if (document.getElementById("O").checked == true) {
        return isItXTurn = false;
    }
}

function loadStatistics() {
    getMatches();
    getDraws();
    getFirstPlayerInfo();
    getSecondPlayerInfo();
}



