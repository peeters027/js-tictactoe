function checkWin(activePlayer) {
    var isThereWin = false;
    if (checkLine(0, 3, 6, activePlayer) || checkLine(1, 4, 7, activePlayer) || checkLine(2, 5, 8, activePlayer) ||
        checkLine(0, 1, 2, activePlayer) || checkLine(3, 4, 5, activePlayer) || checkLine(6, 7, 8, activePlayer) ||
        checkLine(0, 4, 8, activePlayer) || checkLine(6, 4, 2, activePlayer)) {
        isThereWin = true;
    }
    return isThereWin;
}

function checkLine(firstSpot, secondSpot, thirdSpot, activePlayer) {
    var isThereWin = false;
    if (board[firstSpot] === activePlayer && board[secondSpot] === activePlayer && board[thirdSpot] === activePlayer) {
        isThereWin = true;
    }
    return isThereWin;
}

function checkTie() {
    for (i = 0; i < 9; i++) {
        if (board[i] === undefined) {
            return false;
        }
    }
    return true;
}